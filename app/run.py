import logging
import datetime
import argparse

import trigger


def run():
    logging.basicConfig(filename='logs/trigger.log', level=logging.INFO)
    logging.info("ISS Alert Trigger started on {}".format(datetime.datetime.now()))
    trigger.trigger_alert()


if __name__ == '__main__':
    run()
