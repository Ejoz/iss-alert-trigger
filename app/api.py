from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy


# Init app
app = Flask(__name__)
app.config.from_pyfile('config.py')

# Init db
db = SQLAlchemy(app)


# User model
class User(db.Model):

    """ User table """

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    longitude = db.Column(db.Float)
    phone_number = db.Column(db.String(10))

    def __init__(self, name, longitude, phone_number):
        self.name = name
        self.longitude = longitude
        self.phone_number = phone_number

    def serialize(self):
        """ Transform database object in dictionnary for readability

        Returns:
            user {dict}: Dictionnary of user informations
        """

        user = {
            "id": self.id,
            "name": self.name,
            "longitude": self.longitude,
            "phone_number": self.phone_number,
        }

        return user


@app.route('/iss/api/user', methods=['POST'])
def add_user():

    """ Add a user to the database

    body: {
        "name": {str},
        "longitude": {int},
        "phone_number": {str}
    }

    Returns:
        json response: Informations added to the database
    """

    # Check the request validity
    if not request.json or 'name' not in request.json:
        abort(400)

    # Request body
    name = request.json['name']
    longitude = request.json['longitude']
    phone_number = request.json['phone_number']

    # Create a new row in the database
    new_user = User(name, longitude, phone_number)

    # Adds user and updates db
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'New user': User.serialize(new_user)})


@app.route('/iss/api/users', methods=['GET'])
def get_all_users():

    """ Get all users in the database

    Returns:
        json response: list of users response
    """

    all_users = User.query.all()
    users = list()
    for user in all_users:
        u = User.serialize(user)
        users.append(u)

    return jsonify({'Users': users})


@app.route('/iss/api/user/<int:id>', methods=['GET'])
def get_user(id):

    """ Get informations of a unique user

    Args:
        id {int}: User ID

    Returns:
        json response: User informations
    """

    user = User.query.get(id)
    return jsonify({'User': User.serialize(user)})


@app.route('/iss/api/user/<int:id>/longitude', methods=['PUT'])
def update_user_longitude(id):

    """ Update user longitude

    body: {
        "longitude": {int}
    }

    Args:
        id {int}: User ID

    Returns:
        json response: Updated user
    """

    longitude = request.json['longitude']

    user = User.query.get(id)
    user.longitude = longitude
    db.session.commit()

    user_updated = User.serialize(user)

    return jsonify({'Updated user': user_updated})


@app.route('/iss/api/user/<int:id>/phone_number', methods=['PUT'])
def update_user_number(id):

    """ Update user phone number

    body: {
        "phone_number": {str}
    }

    Args:
        id {int}: User ID

    Returns:
        json response: Updated user
    """

    phone_number = request.json['phone_number']

    user = User.query.get(id)
    user.phone_number = phone_number
    db.session.commit()

    user_updated = User.serialize(user)

    return jsonify({'Updated user': user_updated})


@app.route('/iss/api/user/<int:id>', methods=['DELETE'])
def delete_user(id):

    """ Delete the given user

    Args:
        id {int}: User ID

    Returns:
        json response: Confirmation of the deletion
    """
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()

    return jsonify({'User deleted': True})


# Run server
if __name__ == '__main__':
    app.run(debug=True)
