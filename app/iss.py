import requests
import time
from datetime import datetime


def get_current_iss_position():

    """ Get current ISS station position

    Returns:
        latitude: Current ISS latitude
        longitude: Current ISS longitude
    """

    iss_url = 'https://api.wheretheiss.at/v1/satellites/25544'
    response = requests.get(iss_url).json()
    latitude = response['latitude']
    longitude = response['longitude']

    return latitude, longitude
