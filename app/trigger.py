import time
import datetime
import logging
import math

import iss
import config
from api import User

import sqlalchemy as db
from sqlalchemy.orm import sessionmaker

from twilio.rest import Client


def time_now():
    """ Create a readable current time for the log file

    Returns:
        time: Datetime object to display
    """

    time = datetime.datetime.now()
    return time.strftime("%Y-%m-%d %H:%M:%S")


def send_sms_to_user(user_phone_number):

    """ Send SMS to the given user

    Arguments:
        user_phone_number {str}: User phone number
    """

    # Twilio settings
    account_sid = config.TWILIO_ACCOUNT_SID
    auth_token = config.TWILIO_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    message = client.messages.create(
        body="The ISS crossed the set longitude on {}!".format(time_now()),
        from_=config.TWILIO_TRIAL_NUMBER,
        to=user_phone_number
    )


def connect_to_db():

    """ Connection to the database

    Returns:
        connection: Connection tool for query
    """

    # Connection
    engine = db.create_engine(config.SQLALCHEMY_DATABASE_URI)
    connection = engine.connect()

    # Session
    Session = sessionmaker(bind=engine)
    session = Session()

    return connection, session


def get_user_informations(id):

    """ Get user informations

    Args:
        id {int}: ID of the user

    Returns:
        phone_number: Phone number of the given user
        longitude: Longitude of the given user
    """

    connection, session = connect_to_db()
    phone_number, longitude = connection.execute(
        "SELECT phone_number, longitude FROM user WHERE id=={}"
        .format(id)).fetchone()

    return phone_number, longitude


def get_all_users_informations():

    """ Get longitude and phone number from db
        and convert them into a dictionnary

    Returns:
        users: Dictionnary of informations {longitude: phone_number}
    """

    connection, session = connect_to_db()

    longitudes = list()
    phone_numbers = list()

    for user in session.query(User).all():
        longitudes.append(user.__dict__['longitude'])
        phone_numbers.append(user.__dict__['phone_number'])

    users = dict(zip(longitudes, phone_numbers))

    return users


def check_distance(iss_longitude, user_longitude):

    """ Check the distance between the ISS and the user

    Args:
        iss_longitude {float}: ISS longitude
        user_longitude {float}: User longitude

    Returns:
        Bool: True if they are close, False if not
    """

    if abs(iss_longitude - user_longitude) <= 0.1:
        return True
    return False


def trigger_alert():

    """ Triggers an alert when the ISS crosses one of the users longitude.
        Send a SMS if the phone number is Twilio verified.
    """

    users = get_all_users_informations()
    logging.info("There are {} user(s) in the database.".format(len(users)))

    while True:
        iss_latitude, iss_longitude = iss.get_current_iss_position()
        logging.info(
            "ISS position at {}: {} latitude, {} longitude"
            .format(time_now(), iss_latitude, iss_longitude))

        for longitude, phone_number in users.items():
            if check_distance(iss_longitude, longitude):
                logging.info(
                    "The ISS crossed the set longitude on {}!"
                    .format(time_now()))

                try:
                    send_sms_to_user(phone_number)
                except:
                    pass

        time.sleep(2)
        continue
