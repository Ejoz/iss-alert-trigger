# ISS Alert Trigger

The ISS Alert Trigger send you a SMS when the ISS crosses your longitude in your hemisphere.

![ISS image](iss.jpg)
Free of rights (Pixabay)


## Run it yourself

### Install requirements in a virtual environment

```
python3 -m venv .yourvirtualenv
source .yourvirtualenv/bin/activate
pip install -r requirements.txt
```

### Add your informations in the database with the API
You can add your own longitude and phone number with the API provided. You need to run the API with
```
python api.py
```

Then add your informations with a curl request as below:

```
curl --location --request POST 'http://0.0.0.0:5000/iss/api/user' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Your name",
    "longitude": 2.3488,
    "phone_number": "+33333333333"
}'
```

The API response contains your informations:
```
{
  "New user": {
    "id": 1,
    "longitude": 2.3488,
    "name": "Your name",
    "phone_number": "+33333333333"
  }
}
```

### Run the main process
You can now run the streaming process and wait for the ISS to cross your path with
```
python run.py
```

You'll receive the SMS if you posess a [verified Twilio account](https://www.twilio.com/). If not, you'll still be able to see logs in the ```trigger.log``` file.


### Demo
![ISS gif](iss_demo.gif)


## API structure

HTTP method | URI | Action
--- | --- | ---
POST | http://0.0.0.0:5000/iss/api/user | Add a new user to the database
GET | http://0.0.0.0:5000/iss/api/user/id | Get one user from the database
GET | http://0.0.0.0:5000/iss/api/users | Get all users from the database
PUT | http://0.0.0.0:5000/iss/api/user/id/longitude | Update user longitude in the database
PUT | http://0.0.0.0:5000/iss/api/user/id/phone_number | Update user phone number in the database
DELETE | http://0.0.0.0:5000/iss/api/user/id | Delete user from the database
